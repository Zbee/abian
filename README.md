# Abian
Abian is a network for sharing bots for [Adventure Quest Worlds](htpp://aq.com) - made by Artix Entertainment - which is being built from the ground up on [Twitch](https://twitch.tv/zbee_) so that developers without time to look at terrible code can still contribute.

In short, Adventure Quest Worlds is an MMORPG which means that it can get very boring and monotonous. So, you can use bots and a botting client to automate the game, and get through the icky parts and just play the fun parts yourself. However, in order to bots you have to have a bot to use. Since Artix Entertainment really doesn't like bots though, they tend to submit DMCA take down requests to websites that distribute bots. But, since [bots don't use their copyrighted material](http://pastebin.com/QYgp7bTb), these requests are invalid. Regardless of this fact, there are very few bots in circulation outside of groups of friends because no website will host them.

Abian seeks to solve this problem by running a website where people can freely create, host, and share their own bots with others.

[Todo list](https://trello.com/b/deYLVjNm)

## Licensing
Copyright 2015 Ethan Henderson.

[![GPLv3](http://www.gnu.org/graphics/gplv3-88x31.png)](http://www.gnu.org/copyleft/gpl.html)
See the [`COPYING`](https://github.com/Zbee/Abian/blob/master/COPYING) file for the text of this license.

![Attribution 4.0 International](http://i.imgur.com/0PW0UMm.png)
[Abian Documentation](https://github.com/Zbee/Abian/wiki) by Ethan Henderson is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

Utilizes [UserSystem](https://github.com/Zbee/UserSystem/commit/e4c6facb55118638d1ae416da309756b35890fab).

> Copyright (c) 2014-2015 Ethan Henderson;GPLv3 License

Utilizes [reCAPTCHA](http://recaptcha.net).

> Copyright (c) 2007 reCAPTCHA -- http://recaptcha.net; Authors: Mike Crawford and Ben Maurer; MIT License

Utilizes [country-list](https://github.com/umpirsky/country-list/commit/6fbb672c820607af601f1e918cff540f79d9e6bd).

> Copyright (c) Саша Стаменковић <umpirsky@gmail.com>; MIT License

Utilizes [emojify.js](https://github.com/hassankhan/emojify.js/commit/63b8b0528df85912ec43a2eaa3ac0678a414bd56).

> Copyright (c) 2014 Hassan Khan, http://hassankhan.me contact@hassankhan.me;MIT License

Utilizes [Bootstrap](https://github.com/twbs/bootstrap/releases/tag/v3.3.2).

> Code and documentation copyright 2011-2015 Twitter, Inc.; Authors: [mdo](http://github.com/mdo) and [fat](http://github.com/fat); MIT License

Utilizes [Font Awesome](https://github.com/FortAwesome/Font-Awesome/releases/tag/v4.3.0).

> Copyright (c) 2012-2015 [Dave Grandy](https://github.com/davegandy); MIT License

Utilizes [Parsedown](https://github.com/erusev/parsedown/releases/tag/1.5.1).

> Copyright (c) 2013 Emanuil Rusev, erusev.com; MIT License
